from config.config import SETTINGS as s
from db import DB
from datetime import date, timedelta
import MySQLdb

'''
Creates a DB instance and signs in using the config.py file
'''
def connect():
        return DB(s['hostname'], s['port'], s['dbname'], s['username'], s['password'])

'''
cursor connection
'''
def get_connection():
	return MySQLdb.connect(s['hostname'], s['username'], s['password'], s['dbname'])

'''
This function is used to create the table
table_name: is the name of the table to create
params: the paramters to create the table with.
                        ex: name="VARCHAR(20)" would create a field named
                                name with the type VARCHAR(20)
primary_key: a flag to insert a primary key or not
'''
def create_table(table_name, primary_key, **params):
        query = "CREATE TABLE IF NOT EXISTS %s" % table_name
        variables = []
        if (primary_key):
                variables.append(" %s %s " % ('id', "integer PRIMARY KEY AUTO_INCREMENT"))
        for item in params:
                variables.append(" %s %s" % (item, params[item]))
        return query + "( " + ",".join(variables) + " );"

'''
This function is to select data
table_name: is the name of the table to select from
fields: is a list of field names as strings
                ex: ["name", "address", "age"]
filters (Optional): filters on the select statement
                ex: "WHERE age > 40"
'''
def select(table_name, fields, filters=""):
        return "SELECT %s FROM %s %s " % (",".join(fields), table_name, filters)


'''
This function delets from a table
table_name: is the name of the table to delete from
filters (Optional): filters on the delete statement
               ex: "WHERE age > 40"
'''
def delete(table_name, filters=""):
    return "DELETE FROM %s %s;" % (table_name, filters)


'''
This function inserts data into a table
table_name: is the name of table to insert into
params: the parameters to insert
                        ex: name="Alex", address="UMBC"
'''
def insert(table_name, **params):
    columns = []
    values = []
    for key in params:
        columns.append(key)
        values.append("'" + params[key] + "'")

    return "INSERT INTO %s (%s) VALUES (%s);" % (table_name, ",".join(columns), ",".join(values))

'''
This function updates the table
table_name: is the name of table to update
params: the parameters to update
                        ex: name="Alex", address="UMBC"
filters (Optional): filters on the update statement
                ex: "WHERE age > 40"
'''
def update(table_name, filters="", **params):
    updateString = ""
    for key in params:
        updateString = updateString + key + "='" + params[key] + "',"
        updateString = updateString[:-1]

    return "UPDATE %s SET %s %s" % (table_name, updateString, filters)

if __name__ == "__main__":
        db = connect()

        print("Creating table Tasks in the database...")
        # This query will create the table Tasks with the given columns
        query = create_table("Tasks", True, TaskName="VARCHAR(50)", TaskDetails="VARCHAR(200)", AssignedUser="VARCHAR(50)", DateAssigned="date")
        db.execute(query, commit=True)
        print("Created successfully!")
        print


        # this loop will run 20 times and insert 20 rows into the table we created above
        print("Now inserting 20 generated rows into table Tasks...")
        for x in range(1, 21):
		taskNameValue = "Task Name %d" % (x)
		taskDetailsValue = "These are the details for Task Name %d " % (x)
		dateAssignedValue = str(date.today()+timedelta(days=x))
		query = insert("Tasks", TaskName=taskNameValue, TaskDetails=taskDetailsValue, AssignedUser="System", DateAssigned=dateAssignedValue)
		db.execute(query, commit=True)
        print("Rows inserted!")
        print

        # now to select the data, first using id and then date
        print("Selecting all Tasks that have an id greater than 10")

        db.execute(select("Tasks", ['*'], "WHERE id > 10"))
        print_rows(db.get_response())

        print("Selecting all Tasks that are more than three days from now")
        tempDateClause = "WHERE DateAssigned > '%s'" % (str(date.today()+timedelta(days=3)))
        db.execute(select("Tasks", ['*'], tempDateClause))
        print_rows(db.get_response());

        # now updating rows based on conditions
        print("Updating all rows so that tasks assigned 3 days from now are assigned to user Ekjot")
        query = update("Tasks", tempDateClause, AssignedUser="Ekjot")
        db.execute(query)
        print("Rows updated!")
        print

        # now printing the rows that were updated
        print("Selecting and printing all rows assigned to Ekjot")
        db.execute(select("Tasks", ['*'], "WHERE AssignedUser='Ekjot'"))
        print_rows(db.get_response());


        # now we will delete all the rows assigned to Ekjot and then printing out the table
        print("Deleting all rows with Ekjot as the user assigned")
        query = delete("Tasks", "WHERE AssignedUser='Ekjot'")
        db.execute(query)
        print("Rows deleted!")
        print("Printing the table")
        db.execute(select("Tasks", ['*']))
        print_rows(db.get_response())

        print("All tasks completed! :) Now exiting...")

        db.quit()
