from utilities import *

'''
Function that creates the tables in the database for the project.
'''
def run():
	#Create DB Instance
	db = connect()
	print("Creating table in the database...")
	# This query will create the table Tasks with the given columns
	comments = create_table("comments", True, commentText="VARCHAR(200)", parent="VARCHAR(20)", username="VARCHAR(50)", date="date")
	users = create_table("users", True, username="VARCHAR(50)", password="VARCHAR(200)", userid="VARCHAR(20)")
	posts = create_table("posts", True, text="VARCHAR(50)", username="VARCHAR(200)", title="VARCHAR(50)", date="date")
	db.execute(comments, commit=True)
	db.execute(users, commit=True)
	db.execute(posts, commit=True)
	print("Created successfully!")
	print


if __name__ == '__main__':
	run()