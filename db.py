import mysql.connector

class DB:

	'''
	Constructor for DB class
	'''
	def __init__(self, host, port, database_name, username, password):
		self.db = mysql.connector.Connect(host = host, port = port, database = database_name,
											user = username, password = password)
		self.cursor = self.db.cursor()

	'''
	Gets the response from MySQL
	'''
	def get_response(self):
		return self.cursor.fetchall()


	'''
	Executes the query on the MySQL server. Set the commit
	parameter to True when editting anything in order for the
	changes to be made.
	'''
	def execute(self, query, commit=False):
		self.cursor.execute(query)
		if commit:
			self.db.commit()


	'''
	Closes all connections to MySQL server
	'''
	def quit(self):
		self.cursor.close()
		self.db.close()
		