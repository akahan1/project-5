from flask import Flask
from flask import render_template, flash
from flask import request, redirect, url_for
from flask.ext.login import LoginManager, login_user , logout_user , current_user , login_required
import re
import os
from utilities import connect, select, delete, update, insert, get_connection
from datetime import datetime
import MySQLdb
import hashlib

app = Flask(__name__)
app.secret_key = os.urandom(24)

db = connect()

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

'''
The class that encapsulates the functionality of a User of our Blog.
'''
class User():
	def __init__(self, username, password, userid):
		self.username = username
		self.password = password
		self.id = userid
	def is_authenticated(self):
		return True
	def is_active(self):
		return True
	def is_anonymous(self):
		return False
	def get_id(self):
		return unicode(self.id)

'''
Method to get the user object given the id of the user
'''
def getUser(id):
	queryUsers = select("users", ['*'], "WHERE id = '" + id + "'")
	db2 = get_connection()
	cursor = db2.cursor()
	cursor.execute(queryUsers)
	user = cursor.fetchone()
	db2.close()
	try:	
		user = User(user[1], user[2], int(user[0]))
	except (MySQLdb.Error, TypeError) as e:
		return None
	return user

'''
Checks to make sure that the given username and password are valid
'''
def checkLogin(username, password):
	queryUsers = select("users", ['*'], "WHERE username = '" + username + "' and password = '" + password + "'")
	db2 = get_connection()
	cursor = db2.cursor()
	cursor.execute(queryUsers)
	user = cursor.fetchone()
	db2.close()
	try:
		user = User(user[1], user[2], int(user[0]))
	except (MySQLdb.Error, TypeError) as e:
		return None
	return user

'''
Creates a user with the given username and password
'''
def createUser(username, password):
	queryUsers = select("users", ['*'], "WHERE username = '" + username + "'")
	db.execute(queryUsers)
	userExists = db.get_response()
	if len(userExists) > 0:
		return None
	queryInsert = insert("users", username=username,password=password)
	db.execute(queryInsert, commit=True)
	return checkLogin(username, password)
	
'''
Loads the user given the userid
'''
@login_manager.user_loader
def load_user(userid):
	return getUser(userid)

'''
Logs the currently logged in user out of the blog site.
'''
@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect("/login")

'''
The view to log a user in
'''
@app.route("/login", methods=["GET", "POST"])
def login(): 
	if request.method == "GET":
		return render_template("signin.html", userExists=False, error=False)
	if request.form['submit'] == "Login":
		username = request.form["username"]
		password = request.form["password"]
		#hash password for protection
		password = hashlib.sha224(password).hexdigest()
		user = checkLogin(username, password)
		if user is None:
			flash('Username or Password is invalid' , 'error')
			return render_template("signin.html", userExists=False,error=True)
		login_user(user)
		flash('Logged in succesfully!')
		return redirect(request.args.get('next') or url_for('display'))
	else:
		username = request.form["username"]
		password = request.form["password"]
		#hash password for protection
		password = hashlib.sha224(password).hexdigest()
		user = createUser(username, password)
		if user is None:
			return render_template("signin.html", userExists=True, error=False)
		login_user(user)
		return redirect(request.args.get('next') or url_for('display'))

'''
The main view of the application. Displays all blog posts.
'''
@app.route("/", methods=["GET", "POST"])
@login_required
def display():

	#Get all blog posts from db
	querySelect = select("posts", ['*'], "")
	db.execute(querySelect)
	result = db.get_response()
	return render_template("blog.html", posts=result)

'''
Displays information about one specific blog post.
'''
@app.route("/post/<postid>")
@login_required
def blog_post(postid = None):
	if request.method == "GET":
		#look up information of post_id
		querySelect = select("posts", ['*'], "WHERE id = %s" % (postid))
		db.execute(querySelect)
		post = db.get_response()
		

		querySelect = select("comments", ['*'], "WHERE parent = %s" % (postid))
		db.execute(querySelect)
		comments = db.get_response()
		
		return render_template("post.html", post=post[0], comments=comments)

'''
The view containing the page that allows a user to log into the application.
'''
@app.route("/signin", methods=["GET", "POST"])
@login_required
def signin():
	if request.method == "GET":
		return render_template("signin.html")
'''
The view that accepts new posts to be added to the application.
'''
@app.route("/addpost", methods=["GET", "POST"])
@login_required
def addpost():
	if request.method == "GET":
		return render_template("addpost.html")

	elif request.method == "POST":
		postText = MySQLdb.escape_string(request.form.get("postText"))
		postTitle = MySQLdb.escape_string(request.form.get("postTitle"))
		queryInsert = insert("posts", username=current_user.username, text=postText, title=postTitle)
		db.execute(queryInsert, commit=True)
		return redirect(url_for("display"), code=302)

'''
The view that adds a comment to a given post.
'''
@app.route("/addcomment/<postid>", methods=["GET", "POST"])
@login_required
def addcomment(postid=None):
	if request.method == "POST":
		commentText = MySQLdb.escape_string(request.form.get("comment"))
		queryInsert = insert("comments", username=current_user.username, date=str(datetime.now()), commentText=commentText, parent=postid)
		db.execute(queryInsert, commit=True)

	return redirect(url_for("blog_post", postid=postid), code=302)




if __name__ == '__main__':
    app.run(debug=True)
